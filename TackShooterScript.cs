﻿using UnityEngine;
using System.Collections;

public class TackShooterScript : MonoBehaviour {

	public float reloadTime = 0.3f;
	float reload = 0.5f;
	//float bulletImpulse = 1500.0f;
	Camera cam;

	public GameObject bullet_prefab; 

	// Use this for initialization
	void Start () {
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("Start of loop:");
		//Debug.Log (transform.localRotation.eulerAngles);

		if(reload > 0) {
			reload -= Time.deltaTime;
		}

		if(Input.GetButton("Fire1") && reload <= 0) {
			// Reset firing delay
			reload = reloadTime;
			//wwwwwwae is aiming and point muzzle in that direction
			Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

			RaycastHit hit;

			if (Physics.Raycast(ray, out hit)) {
				// Make sure we don't aim at other bullets that we just fired.
				if (hit.transform.tag != "TackBullet") {
					transform.LookAt(hit.point);
				}
			}
			else {
				transform.localRotation = Quaternion.LookRotation(new Vector3(0f, 0f, 0f));
				//Debug.Log (transform.localRotation.eulerAngles);

			}

			// Spawn bullet
			//GameObject bullet = (GameObject)
			Instantiate(bullet_prefab, transform.position, transform.rotation);

			//Debug.Log (transform.localRotation.eulerAngles);

			// Make bullet move
			//bullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletImpulse, ForceMode.Impulse);

			//Debug.Log ("End of loop:");
			//Debug.Log (transform.localRotation.eulerAngles);
		}
	}

	Quaternion BillboardFaceCam(Transform theTransform, Camera ca) {
		// Returns rotation parallel to camera cam.
		Vector3 direction;
		Quaternion look;

		direction = ca.transform.forward;
		
		look = Quaternion.LookRotation(direction);
		
		return look;
	}
}

﻿using UnityEngine;
using System.Collections;

public class FPSController : MonoBehaviour {

	float desiredVertical = 0;

	public float movementSpeed = 12.0f;
	public float mouseSense = 5.0f;
	public float verticalRange = 89.0f;

	// Initialize input variables.
	float mouseX = 0.0f;
	float mouseY = 0.0f;
	float forwardSpeed = 0.0f;
	float strafeSpeed = 0.0f;

	CharacterController cc;

	Vector3 speed = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {
		cc = GetComponent<CharacterController>();

		// Lock the mouse
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;
	}
	
	// Update is called once per frame.
	void Update () {


		// Fetch input values.
		ProcessInput();



		transform.Rotate (0, mouseX, 0);

		desiredVertical -= mouseY;

		desiredVertical = Mathf.Clamp (desiredVertical, -verticalRange, verticalRange);
		
		Camera.main.transform.localRotation = Quaternion.Euler(desiredVertical, 0, 0);

		speed = new Vector3((strafeSpeed * movementSpeed), Physics.gravity.y, (forwardSpeed * movementSpeed));

		speed = transform.rotation * speed;

		cc.Move(speed * Time.deltaTime);
	}
		
	void ProcessInput() {
		// Lock mouse on click
		if(Input.GetMouseButtonDown(0)) {
			if((Cursor.lockState != CursorLockMode.Locked) || (Cursor.visible != false)) {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
		}

		// Populate input variables.
		mouseX = Input.GetAxis ("Mouse X") * mouseSense;
		mouseY = Input.GetAxis ("Mouse Y") * mouseSense;
		forwardSpeed = Input.GetAxis("Vertical");
		strafeSpeed = Input.GetAxis("Horizontal");
	}

}
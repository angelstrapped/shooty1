using UnityEngine;
using System.Collections;

public class PenStuck : MonoBehaviour {
	public float lifeSpan = 30.0f;

    public float knockbackTimer = 1.0f;
    public CharacterController parentEnemy;
	
	void Start () {
        parentEnemy = FindCharacterControllerInParents(transform);
	}

	void Update () {
		lifeSpan -= Time.deltaTime;

        if (parentEnemy) {
            KnockBack();
        }

		if (lifeSpan <= 0) {
			Destroy(gameObject);
		}
	}

    void KnockBack() {
        if (knockbackTimer > 0.1) {
            knockbackTimer -= Time.deltaTime;
            parentEnemy.Move(transform.forward * knockbackTimer);
        }
    }

    CharacterController FindCharacterControllerInParents(Transform parentTransform) {
        Debug.Log("parentTransform: " + parentTransform);
        Debug.Log("GetComponent: " + parentTransform.GetComponent<CharacterController>());
        Debug.Log("GetComponent == null: " + (parentTransform.GetComponent<CharacterController>() == null));
        if (parentTransform == null) {
            Debug.Log("no more parents");
            return default(CharacterController);
        } else if (parentTransform.GetComponent<CharacterController>() == null) {
            Debug.Log("recurse");
            return FindCharacterControllerInParents(parentTransform.parent);
        } else {
            Debug.Log("found");
            return parentTransform.GetComponent<CharacterController>();
        }
    }

    /* // error CS0019: Operator '==' cannot be applied to operands of type 'T' and 'T'
    T FindComponentInParents<T>(Transform parentTransform) {
        Debug.Log("parentTransform: " + parentTransform);
        Debug.Log("GetComponent: " + parentTransform.GetComponent<T>());
        Debug.Log("GetComponent == null: " + (parentTransform.GetComponent<T>() == null));
        if (parentTransform == null) {
            Debug.Log("null");
            return default(T);
        } else if (parentTransform.GetComponent<T>() == default(T)) {
            Debug.Log("recurse");
            return FindComponentInParents<T>(parentTransform.parent);
        } else {
            Debug.Log("found");
            return parentTransform.GetComponent<T>();
        }
    } */

    // GetComponent returns null, but GetComponent == null is false.
    /* T FindComponentInParents<T>(Transform parentTransform) {
        Debug.Log("parentTransform: " + parentTransform);
        Debug.Log("GetComponent: " + parentTransform.GetComponent<T>());
        Debug.Log("GetComponent == null: " + (parentTransform.GetComponent<T>() == null));
        if (parentTransform == null) {
            Debug.Log("null");
            return default(T);
        } else if (parentTransform.GetComponent<T>() == null) {
            Debug.Log("recurse");
            return FindComponentInParents<T>(parentTransform.parent);
        } else {
            Debug.Log("found");
            return parentTransform.GetComponent<T>();
        }
    } */
}

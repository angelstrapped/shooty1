﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class WeaponSwitchingScript : MonoBehaviour {

	public List<GameObject> weapons = new List<GameObject>();

	public int numWeapons;

	public int currentWeapon = 0;

	void Start() {
		weapons.AddRange(FindChildrenWithTag("Weapon"));
		Debug.Log (weapons[0]);
		Debug.Log (weapons[1]);

		foreach (GameObject weapon in weapons) {
			weapon.SetActive(false);
		}

		weapons[0].SetActive(true);

		numWeapons = weapons.Count;
	}

	void Update() {
		float scroll = Input.GetAxis("Mouse ScrollWheel");

		if (scroll < 0.0f) {
			SwitchWeapon(-1);
		}
		else if (scroll > 0.0f) {
			SwitchWeapon(1);
		}
	}

	void SwitchWeapon(int scrollDirection) {
		// Deactivate the currently active weapon.
		weapons[currentWeapon].SetActive(false);

		// Choose the new weapon index.
		// Still some kludgy shit going on here. Oh well.
		currentWeapon = (currentWeapon + scrollDirection) % numWeapons;

		if ( currentWeapon == -1 ) {
			currentWeapon = numWeapons -1;
		}

		Debug.Log(currentWeapon);
		// Activate the new weapon.
		weapons[currentWeapon].SetActive(true);
	}

	List<GameObject> FindChildrenWithTag(string tag) {
		List<GameObject> foundChildren = new List<GameObject>();

		foreach (Transform child in transform) {
			if (child.tag == tag) {
				foundChildren.Add(child.gameObject);
			}
		}

		return foundChildren;
	}
}

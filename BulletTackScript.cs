﻿using UnityEngine;
using System.Collections;

public class BulletTackScript : MonoBehaviour {

	public GameObject stuckTackObject;
	private float lifeSpan = 3.0f;
	private float speed = 16.0f;
	private bool flying = true;
	
	void Update () {
		Fly();
	}

	void OnTriggerEnter(Collider collision) {
		if (collision.gameObject.name != "Player") {
			// Instantiate a stuck tack, essentially a bullet hole.
			GameObject stuckTack = (GameObject)Instantiate(stuckTackObject, this.transform.position, this.transform.rotation);
			stuckTack.transform.parent = collision.gameObject.transform;
			// Destroy the actual bullet.
			Destroy(this.gameObject);
			Debug.Log("Bollision");
		}

	}

	void Fly() {
		if (flying) {
			this.transform.Translate(Vector3.forward * speed * Time.deltaTime);

			lifeSpan -= Time.deltaTime;

			// If lifeSpan reaches 0 and we're still flying, destroy the bullet.
			if (lifeSpan <= 0) {
				Destroy(gameObject);
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class FPSPantsController : MonoBehaviour {
	public float gravity = Physics.gravity.y * 5;
	float desiredVertical = 0;

	public float movementSpeed = 12.0f;
	public float mouseSense = 5.0f;
	public float verticalRange = 89.0f;

	CharacterController cc;

	Vector3 speed = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {
		cc = GetComponent<CharacterController>();

		// Lock the mouse
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonDown(0)) {
			if((Cursor.lockState != CursorLockMode.Locked) || (Cursor.visible != false)) {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
		}

		float mouseX = Input.GetAxis ("Mouse X") * mouseSense;

		transform.Rotate (0, mouseX, 0);

		float mouseY = Input.GetAxis ("Mouse Y") * mouseSense;

		desiredVertical -= mouseY;

		desiredVertical = Mathf.Clamp (desiredVertical, -verticalRange, verticalRange);
		
		Camera.main.transform.localRotation = Quaternion.Euler(desiredVertical, 0, 0);

		// Movement
		float forwardSpeed = Input.GetAxis("Vertical");

		float strafeSpeed = Input.GetAxis("Horizontal");

		speed = new Vector3((strafeSpeed * movementSpeed), Physics.gravity.y, (forwardSpeed * movementSpeed));

		speed = transform.rotation * speed;

		cc.Move(speed * Time.deltaTime);
	}
}

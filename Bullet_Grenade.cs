﻿using UnityEngine;
using System.Collections;

public class Bullet_Grenade : MonoBehaviour {

	float lifeSpan = 3.0f;

	public GameObject boom_prefab; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		lifeSpan -= Time.deltaTime;

		if(lifeSpan <= 0) {
			Explode();
		}
	}

	void OnCollisionEnter(Collision collision) {
		if(collision.gameObject.tag == "Enemy") {
			Explode();
		}
	}

	void Explode() {
		Instantiate(boom_prefab, new Vector3(transform.position.x, transform.position.y + 0.9f, transform.position.z), transform.rotation);
		Destroy(gameObject);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Shooty: MonoBehaviour {

	float reloadTime = 0.7f;
	float reload = 0.5f;
	float bulletImpulse = 1000.0f;
	Camera cam;

	public GameObject bullet_prefab; 

	// Use this for initialization
	void Start () {
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("Start of loop:");
		//Debug.Log (transform.localRotation.eulerAngles);

		if(reload > 0) {
			reload -= Time.deltaTime;
		}



		if(Input.GetButton("Fire1") && reload <= 0) {
			// Reset firing delay
			reload = reloadTime;

			// Find where reticule is aiming and point muzzle in that direction
			Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

			RaycastHit hit;

			if (Physics.Raycast(ray, out hit)) {

				transform.LookAt(hit.point);
				//Debug.Log ("Inside if:");
				//Debug.Log (transform.localRotation.eulerAngles);
			}
			else {
				transform.localRotation = Quaternion.LookRotation(new Vector3(0f, 0f, 0f));
				//Debug.Log (transform.localRotation.eulerAngles);

			}

			// Spawn bullet
			GameObject bullet = (GameObject)Instantiate(bullet_prefab, transform.position, BillboardFaceCam(transform, cam));

			//Debug.Log (transform.localRotation.eulerAngles);

			// Make bullet move
			bullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletImpulse, ForceMode.Impulse);

			//Debug.Log ("End of loop:");
			//Debug.Log (transform.localRotation.eulerAngles);
		}
	}

	Quaternion BillboardFaceCam(Transform trans, Camera ca) {
		// Returns rotation parallel to camera cam.
		Vector3 direction;
		Quaternion look;

		direction = ca.transform.forward;
		direction.y = 0;
		
		look = Quaternion.LookRotation(direction);
		
		return look;
	}
}

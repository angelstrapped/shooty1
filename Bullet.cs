using UnityEngine;
using System.Collections;

public abstract class Bullet : MonoBehaviour {
	public GameObject stuckTackObject;

	void Update () {
		Fly();
	}

	protected abstract void OnTriggerEnter(Collider collision);

	protected abstract void Fly();
}

﻿using UnityEngine;
using System.Collections;

public class EnemyFPSController : MonoBehaviour {
	public float gravity = Physics.gravity.y * 5;

	public float strafeDirection = 1.0f;
	float movementSpeed = 4.0f;
	public float mouseSense = 5.0f;
	public float verticalRange = 89.0f;

	int x1 = 7;
	int x2 = 14;

	// Initialize input variables.
	float mouseX = 0.0f;
	float mouseY = 0.0f;
	float forwardSpeed = 0.0f;
	float strafeSpeed = 0.0f;

	public float targetDistance = 0.0f;

	public string state = "idle";

	public float stateTimer = 0.0f;

	public float brainTimer = 0.0f;

	public bool turn = false;

	public float angleToPlayer = 0.0f;

	public GameObject target;

	public EnemyTackShooterScript shooterScript;

	CharacterController cc;

	Vector3 speed = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {
		cc = GetComponent<CharacterController>();

		target = AcquireTarget();

		InvokeRepeating("newStrafeDirection", 1.0f, 1.0f);
	}

	// Update is called once per frame.
	void Update () {
		// Count up deltatime.
		stateTimer += Time.deltaTime;
		brainTimer += Time.deltaTime;

		// Fetch input values.
		ProcessInput();
		
		if ( state == "idle" ) {
			Idle();
			return;
		} else if ( state == "alert" ) {
			Alert();
		} else if ( state == "approach" ) {
			Approach();
		} else if ( state == "retreat" ) {
			Retreat();
		} else if ( state == "attack" ) {
			Attack();
		} else if ( state == "dodge" ) {
			Dodge();
		} else if ( state == "think" ) {
			Think();
		}

		if (turn == true) {
			transform.Rotate (0, mouseX, 0);
		}

		speed = new Vector3((strafeSpeed * movementSpeed), gravity, (forwardSpeed * movementSpeed));

		speed = transform.rotation * speed;

		cc.Move(speed * Time.deltaTime);
	}

	void newStrafeDirection() {
		Ray ray = new Ray(this.transform.position, this.transform.right * strafeDirection);

		bool obstructed = Physics.Raycast(ray.origin, ray.direction, 1.0f);

		if (obstructed) {
			strafeDirection = -strafeDirection;
		}
	}

	void ProcessInput() {
		// Populate input variables.
		Vector3 targ = new Vector3(target.transform.position.x, 0, target.transform.position.z);
		Vector3 me = new Vector3(this.transform.position.x, 0, this.transform.position.z);

		angleToPlayer = turnTowardPlayer(targ, me, 2.0f);

		mouseX = angleToPlayer;
	}

	void getTargetDistance() {
		Vector3 targ = new Vector3(target.transform.position.x, 0, target.transform.position.z);
		Vector3 me = new Vector3(this.transform.position.x, 0, this.transform.position.z);

		targetDistance = Vector3.Distance(targ, me);
	}

	void Idle() {
		// If it's been half a second, look for the target.
		if ( stateTimer > 0.5f ) {
			Debug.Log("idling");
			stateTimer = 0.0f;

			getTargetDistance();

			// If target is in range, go to attack state.
			if ( targetDistance < x2 ) {
				if ( Physics.Raycast(this.transform.position, target.transform.position - this.transform.position) ) {
					Debug.Log("found target.");
					state = "alert";
					stateTimer = 0.6f;
					return;
				}
			}
		}
	}

	void Alert() {
		turn = true;

		if ( stateTimer > 0.5f ) {
			stateTimer = 0.0f;

			if ( brainTimer > 2.0f ) {
				brainTimer = 0.0f;
				state = "think";
				return;
			}

			getTargetDistance();

			if ( targetDistance > x2 ) {
				state = "approach";

			} else if ( targetDistance < x1 ) {
				state = "retreat";

			} else {
				state = "dodge";
			}
		}
	}

	void Think() {
		strafeSpeed = 0.0f;
		forwardSpeed = 0.0f;

		if ( stateTimer > 1.0f ) {
			state = "alert";
		}

	}

	void Attack() {
		// Stay in attack state until timer is up.
		if ( stateTimer > 0.5f ) {
			stateTimer = 0.0f;

			// Tell the gun to shoot, it handles re-fire delays and so on.
			if ( shooterScript.Shoot() ) {
				// Exit attack state now that we have successfully fired a shot.
				state = "alert";
			}
		}
	}

	void Dodge() {
		turn = true;

		if ( stateTimer > 0.5f ) {
			stateTimer = 0.0f;

			forwardSpeed = 0.0f;

			if ( angleToPlayer > 0 ) {
				strafeSpeed = 1.0f;
			} else {
				strafeSpeed = -1.0f;
			}

			state = "attack";
		}
	}

	void Approach() {
		turn = false;

		if ( stateTimer > 0.5f ) {
			stateTimer = 0.0f;

			forwardSpeed = 1.0f;

			state = "attack";
		}
	}

	void Retreat() {
		turn = false;

		if ( stateTimer > 0.5f ) {
			stateTimer = 0.0f;

			forwardSpeed = -1.0f;

			state = "attack";
		}
	}

	// Returns a signed angle from direction me to direction targ.
	float turnTowardPlayer(Vector3 targ, Vector3 me, float degrees) {
		// Get unsigned angle between this object's forward and player's position.
		float unsignedAngle = Vector3.Angle( targ - me, this.transform.forward);

		// Calculate the sign (i.e. is the player to my left or my right.)
		float sign = ( Vector3.Cross(targ - me, this.transform.forward).y > 0 ? -1 : 1 );

		// Clamp rotation amount. Do this the easy way, i.e before signing.
		unsignedAngle = Mathf.Min(unsignedAngle, 2.0f);

		// Apply correct sign to rotation amount.
		return unsignedAngle * sign;
	}

	GameObject AcquireTarget() {
		return GameObject.Find("Player");
	}
}


﻿using UnityEngine;
using System.Collections;

public class EnemyTopScript : MonoBehaviour {

	public int health = 5;

	public EnemyScript enemyMain;

	void Update () {
		if (health < 1) {
			TopHalfDie();
		}
	}

	void TopHalfDie() {
		Destroy(gameObject);
	}

	void OnTriggerEnter(Collider hit) {
		//Debug.Log("Something happen");
		if (hit.gameObject.tag == "explosion") {
			Debug.Log("Top was hit!");
			health -= 2;
			enemyMain.TakeDamage(2, hit.GetInstanceID());
		}
		else if (hit.gameObject.tag == "TackBullet") {
			Debug.Log("Top was hit!");
			health -= 1;
			enemyMain.TakeDamage(1, hit.GetInstanceID());
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

	public int hitpoints;
	private ArrayList bulletsThatHit = new ArrayList();

	// Use this for initialization
	void Start () {
		hitpoints = 10;
	}
	
	// Update is called once per frame
	void Update () {
		if (hitpoints <= 0) {
			Debug.Log("Ded.");
			Destroy(this.gameObject);
		}
	}

	public void TakeDamage(int damage, int uniqueID) {
		// Check to see if we have taken damage from this bullet before.
		if ( !bulletsThatHit.Contains(uniqueID) ) {
			// Add bullet to list of bullets that have damaged us.
			bulletsThatHit.Add(uniqueID);
			hitpoints -= damage;
		}

	}
}

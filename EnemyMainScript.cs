﻿using UnityEngine;
using System.Collections;

public class EnemyMainScript : MonoBehaviour {

	public EnemyScript enemyMain;
	public int health = 10;

	void Update () {
		if (health < 1) {
			Die();
		}
	}

	// Happens when health reaches 0.
	void Die() {
		Debug.Log("Ded.");
		Destroy(this.transform.parent.gameObject);
	}

	void OnTriggerEnter(Collider hit) {
		//Debug.Log("Something happen");
		if (hit.gameObject.tag == "explosion") {
			Debug.Log("Enemy was hit!");
			health -= 2;
			enemyMain.TakeDamage(2, hit.GetInstanceID());
		}
		else if (hit.gameObject.tag == "TackBullet") {
			Debug.Log("Enemy was hit!");
			health -= 1;
			enemyMain.TakeDamage(1, hit.GetInstanceID());
		}
	}
}

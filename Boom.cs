﻿using UnityEngine;
using System.Collections;

public class Boom : MonoBehaviour {

	float lifeSpan = 0.4f;

	Camera cam;
	// Use this for initialization
	void Start () {
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {

		lifeSpan -= Time.deltaTime;

		if(lifeSpan <= 0) {
			Destroy(gameObject);
		}

		transform.rotation = BillboardFaceCam(transform, cam);

		// Points the "billboard" toward the camera, not in the direction the camera is facing.
		//direction = cam.transform.position;
		//direction.y = transform.position.y;
		//transform.LookAt(direction);

	}

	Quaternion BillboardFaceCam(Transform transform, Camera cam) {
		// Returns rotation parallel to camera cam.
		Vector3 direction;
		Quaternion look;

		transform.rotation = cam.transform.rotation;
		direction = cam.transform.forward;
		direction.y = 0;

		look = Quaternion.LookRotation(direction);
		
		return look;
	}
}

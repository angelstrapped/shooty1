using UnityEngine;
using System.Collections;

public class EnemyTackShooterScript : MonoBehaviour {

	float reloadTime = 0.9f;
	float reload = 0.5f;

	public GameObject bullet_prefab; 

	void Start () {
	}

	void Update () {
		if(reload > 0) {
			reload -= Time.deltaTime;
		}
	}

	public bool Shoot() {
		if(reload <= 0) {
			reload = reloadTime;
			Instantiate(bullet_prefab, transform.position, transform.rotation);
			return true;
		} else {
			return false;
		}
	}

}
